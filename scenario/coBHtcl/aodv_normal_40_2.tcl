#Set all the options

set val(chan)         Channel/WirelessChannel  ;# channel type
set val(prop)         Propagation/TwoRayGround ;# radio-propagation model
set val(ant)          Antenna/OmniAntenna      ;# Antenna type
set val(ll)           LL                       ;# Link layer type
set val(ifq)          Queue/DropTail/PriQueue  ;# Interface queue type
set val(ifqlen)       50                       ;# max packet in ifq
set val(netif)        Phy/WirelessPhy          ;# network interface type
set val(mac)          Mac/802_11               ;# MAC type
set val(rp)           AODV                     ;# routing protocol
set val(x)            800		       ;# X length
set val(y)            800		       ;# Y length
set val(finish)       500		       ;# Finish time
set val(nn)           40		       ;# number of mobilenodes

set val(dis) 	      100

set val(ni)	"nodemovement"
set val(nc)	"traffic"
set ns_ [new Simulator]

set f [open out.tr w]
$ns_ trace-all $f 
set namtrace [open out.nam w]
$ns_ namtrace-all-wireless $namtrace $val(x) $val(y)
set topo [new Topography]
$topo load_flatgrid $val(x) $val(y)
 
set god_ [create-god $val(nn)]
set chan_1 [new $val(chan)]

# CONFIGURE AND CREATE NODES

$ns_ node-config  -adhocRouting $val(rp) \
          -llType $val(ll) \
                 -macType $val(mac) \
                 -ifqType $val(ifq) \
                 -ifqLen $val(ifqlen) \
                 -antType $val(ant) \
                 -propType $val(prop) \
                 -phyType $val(netif) \
                 -topoInstance $topo \
                 -agentTrace ON \
                 -routerTrace ON \
                 -macTrace ON \
                 -movementTrace ON \
                 -channel $chan_1

for {set i 0} {$i < $val(nn) } { incr i } {
        set node_($i) [$ns_ node]
 $ns_ initial_node_pos $node_($i) 35
    }


proc finish {} {
    global ns_ namtrace filename
    $ns_ flush-trace
    close $namtrace  
    #exec nam out.nam &
    exit 0
}

set myagent [new Agent/MyAgent]
$myagent set malicious_1 3

$myagent myfunc

#source mob

source $val(ni)
source $val(nc)

$ns_ at 0.0 "$node_(1) color blue"
$node_(1) color "blue"
$ns_ at 0.0 "$node_(2) color orange"
$node_(2) color "blue"
$ns_ at 0.0 "$node_(3) color red"
$node_(3) color "red"


set udp_(0) [new Agent/UDP]
$ns_ attach-agent $node_(38) $udp_(0)
set null_(0) [new Agent/Null]
$ns_ attach-agent $node_(39) $null_(0)
set cbr_(0) [new Application/Traffic/CBR]
$cbr_(0) set packetSize_ 512
$cbr_(0) set interval_ 0.2
$cbr_(0) set random_ 1
$cbr_(0) attach-agent $udp_(0)
$ns_ connect $udp_(0) $null_(0)

$ns_ at 100 "$cbr_(0) start"
$ns_ at 130 "$cbr_(0) stop"

#$ns_ at 140 "$cbr_(0) start"
#ns_ at 170 "$cbr_(0) stop"

#$ns_ at 180 "$cbr_(0) start"
#$ns_ at 210 "$cbr_(0) stop"

$ns_ at $val(finish) "finish"
puts "Start of simulation..."
$ns_ run
